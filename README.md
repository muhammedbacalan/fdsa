# Frontend Device Setup Automation

Uses Ansible.

It just works and it's ~~effing~~ amazing!

## Usage

* Use a Linux distrobution like a sensible developer

* Install Ansible (don't ask me how)

* Install git

* `ansible-pull -U https://muhammedbacalan@bitbucket.org/muhammedbacalan/fdsa.git`

* Be amazed

## Details

### Packages installed from apt

I'm listing these separately so we know what to replace when we want to use this
with another distrobution:

- git
- zsh
- curl
- xclip
- htop
- nodejs
- npm

git, zsh, curl, nodejs and npm needs no explanation.

git should already be installed to be able to use `ansible-pull`,  
I added it again here in case it's out of date.  

`xclip` is a CLI for clipboard management.  
It allows you to use programatically copy things to clipboard.  
I don't use it on my own but it's needed for setting up or configuring other things so I decided to include it.

[htop](https://htop.dev/) is a better alternative to built-in `top`, a process viewer.

I'm aware some of these packages come pre-installed on Ubuntu.  
I listed them explicitly anyways, in case some people uses different flavors  
or things change in different versions of Ubuntu.  

### Other packages

- [ohmyzsh](https://ohmyz.sh/)
- [Visual Studio Code](https://code.visualstudio.com/) - *I use Vim BTW*
- [Slack](https://slack.com)
- [Chromium](https://www.chromium.org/Home/) - *I use Firefox BTW*

## Node version management

WIP.

I normally use nvm but had some issues setting it up via Ansible.  
Mostly because we will be executing `ansible-pull` in bash but once everything is installed,  
we will be using zsh and nvm will have be re-configured to export the paths correctly.

## Suggestions & Improvements

Feel free to open an issue if you know some other cool packages to add  
or if you have improvements for the process etc.

Just keep in mind that this is targeting frontend development specifically  
and included packages should be generally useful things, not personal preferences.

This has been tested on Ubuntu 20.04 LTS.

---

*So where is my promotion?*

